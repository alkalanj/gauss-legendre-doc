'use strict';

const fs = require('fs');
const path = require('path');
const qprintf = require('qprintf');

const dirname = process.argv[2];
const files = fs.readdirSync(dirname);

let latex = '';

files.sort((a, b) => parseInt(a) - parseInt(b));

files.forEach(file => {
    if (file.indexOf('.json') === -1) return;
    const content = fs.readFileSync(path.join(dirname, file), 'utf-8');
    const quadParams = JSON.parse(content);
    latex += toLatex(quadParams);
});

console.log(latex);

function toLatex(params) {
    const n = params.weights.length;
    let latex = '';
    for (let i = 0; i < n / 2; i++) {
        latex += qprintf.sprintf("    %3d & $\\pm %1.15f$ & %1.15f \\\\ \n",
            (i === 0 ? n : ' '),
            Math.abs(params.abscissas[i]),
            params.weights[i]);
    }
    latex += "    \\hline \n";
    return latex;
}
